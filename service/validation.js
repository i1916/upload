const { validationResult } = require("express-validator");

exports.insertValidation = (req) => {
    return new Promise((resolve, reject) => {
        var errors = validationResult(req);
        if (!errors.isEmpty()) {
            reject(errors);
        } else {
            resolve();
        }
    })
}

exports.updateValidation = (req) => {
    return new Promise((resolve, reject) => {
        var errors = validationResult(req);
        if (!errors.isEmpty()) {
            reject(errors);
        } else {
            resolve();
        }
    })
}

exports.fetchByIdValidation = (req) => {
    return new Promise((resolve, reject) => {
        var errors = validationResult(req);
        if (!errors.isEmpty()) {
            reject(errors);
        } else {
            resolve();
        }
    })
}