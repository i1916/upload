const mysql = require('mysql');
const {dbConfig} = require(`../credentials/env`);

var mysqlConnection = mysql.createConnection({
    host: dbConfig.host,
    user: dbConfig.user,
    password: dbConfig.password,
    database: dbConfig.database,
    multipleStatements: dbConfig.multiple
});

mysqlConnection.connect((err) => {
    if(!err) {
        console.log("CONNECTED");
    } else {
        console.log("Connection Failed..!!!:"+ JSON.stringify(err,undefined,2));
    }
})

module.exports = mysqlConnection;