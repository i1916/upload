const express = require("express");
const { param, body } = require('express-validator');
const { updateById, uploadFile } = require("../controller/crudController");
const Router = express.Router();
const path = require('path');
const multer = require('multer');



const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'Uploads');
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, new Date().toISOString().replace(/:/g, '-') + path.extname(file.originalname))
    }
})

const upload = multer({storage: storage});

// upload router
Router.post("/upload",upload.single('image'), uploadFile);


// update the data by id
Router.put("/updatecustomer/:id", [
    param('id').notEmpty().isInt().withMessage('enter a valid id'),
    body('name').notEmpty().withMessage('should not be empty').isLength({ min: 6, max: 30 }),
    body('address').notEmpty().withMessage('should not be empty').isLength({ min: 5, max: 50 }),
    body('phone_number').notEmpty().withMessage('should not be empty').isLength({ min: 8, max: 12 }),
], updateById);


module.exports = Router;