const envName = (process.env.NODE_ENV) ? process.env.NODE_ENV : "development";

console.log(`server running on ${envName}`);
const activeEnv = require(`../environment/${envName}`);


const dbConfig = {
    host: activeEnv.host,
    user: activeEnv.user,
    password: activeEnv.password,
    database: activeEnv.database,
    multiple: activeEnv.multipleStatements
};

module.exports= {dbConfig};