
const mysqlConnection = require("../config/dbConnection");

exports.insertData = (cust) => {
    return new Promise((resolve, reject) => {
        var querys = `INSERT INTO customers (name, address, phone_number) VALUES ('${cust.name}', '${cust.address}', ${cust.phone_number})`;
        mysqlConnection.query(querys, (err, result, fields) => {
            if (result.affectedRows !== 0 || result.affectedRows !== "") {
                resolve(result);
                console.log(result.affectedRows + " record(s) inserted");
            } else {
                reject(err);
            }
        })
    })
}


exports.updateData = (id, objData) => {
    return new Promise((resolve, reject) => {
        // console.log(`------------>,${id}`);
        // console.log("1234", objData);
        const sql1 = `SELECT * FROM customers WHERE Customerid = ${id}`;
        mysqlConnection.query(sql1, (err, result, fields) => {
            if (result.length == 0) {
                reject({ err: "No Data Found...." })
            } else {
                var sql = `UPDATE customers SET name = '${objData.name}', address= '${objData.address}', phone_number= ${objData.phone_number} WHERE Customerid = ${id}`;
                mysqlConnection.query(sql, (err, results, fields) => {
                    resolve({ data: results });
                })
            }
        })
    })
}


exports.getAllData = () => {
    return new Promise((resolve, reject) => {
        var querys = `SELECT * from customers`;
        mysqlConnection.query(querys, (err, result, fields) => {
            if (result !== 0 || result !== "") {
                resolve(result);
            } else {
                reject(err);
            }
        })
    })
}


exports.getById = (id) => {
    return new Promise((resolve, reject) => {
        mysqlConnection.query(`SELECT * from customers WHERE Customerid = ${id}`, (err, result, fields) => {
            if (result == 0 || result == "") {
                reject(err);
            } else {
                resolve(result);
            }
        })
    })
}


exports.singleDelete = (id) => {
    return new Promise((resolve, reject) => {
        mysqlConnection.query(`DELETE FROM customers WHERE Customerid = ${id}`, (err, result, fields) => {
            if (result.affectedRows == 0) {
                reject({ error: err });
            } else {
                resolve({ data: result });
            }
        })
    })
}