const app = require('../app');
const bodyParser = require("body-parser");
 require('dotenv').config();

app.use(bodyParser.json());
var routes = require("../router/crudRouter");
app.use("/api", routes);


app.listen(process.env.PORT, () => {
    console.log('Server is running on port no. :---> ', process.env.PORT);
});