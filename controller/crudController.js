const fs = require('fs');

//uploading a json with increasing price
exports.uploadFile = (req, res) => {
    const jsonData = require(`../Uploads/${req.file.filename}`);
    var ab = JSON.stringify(jsonData);
    var cd = JSON.parse(ab);
    // res.status(200).json({msg:"Data UPLOADED...", data: cd});
    const incValue = req.body.incCount;
    console.log("incCount==>", incValue);
    var arr = [];
    for (var [key, value] of Object.entries(cd)){
        var keys = `${key}`;
        var increaseValue = `${+value+parseInt(incValue)}`;
        var objData = `${keys}: ${increaseValue}`;
        // console.log("-------->>>>>>>", objData);
        arr.push(objData);
    }
    var tab = {"increasedData":arr};
    var yu = JSON.stringify(tab);
    fs.writeFileSync(`uploads/${req.file.filename}`, yu, function(err) {     
        if (err) throw err;
        console.log("Data is written to file successfully.")
    });
    fs.readFile(`uploads/${req.file.filename}`, "utf-8", (err, data) => {
        const medData = JSON.parse(data);
        res.status(200).json({info: medData.increasedData});
    });
}